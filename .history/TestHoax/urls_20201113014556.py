from django.urls import path

from . import views

app_name = 'TestHoax'

urlpatterns = [
    path('testhoax/', views.testhoax, name='testhoax'),
    path('soal/', views.soal, name='soal'),
    path('soal2/<int:nomor>/<int:skor>', views.soal, name='soal2'),
    path('soal3/<int:nomor>/<int:skor>', views.soal, name='soal3'),
    path('soal4/<int:nomor>/<int:skor>', views.soal, name='soal4'),
    path('soal5/<int:nomor>/<int:skor>', views.soal, name='soal5'),
    path('skorhasil/<int:nomor>/<int:skor>', views.skor, name='skor'),
]
