from django.test import TestCase, Client
from .models import JawabanUser

# Create your tests here.
class TestWeb(TestCase):

    def test_template_home(self):
        response = Client().get('/TestHoax/testhoax')
        self.assertTemplateUsed(response, "home2.html")

    def test_models_JawabanUser(self):
        var = JawabanUser.objects.create(Jawaban="select1", Jawaban_ID=1)
        self.assertEquals(var.Jawaban, "select1")
        self.assertEquals(var.Jawaban_ID, 1)

    