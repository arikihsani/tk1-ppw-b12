from django.test import TestCase, Client
from .models import JawabanUser
from django.urls import reverse

# Create your tests here.
class TestWeb(TestCase):

    def test_template_home(self):
        response = Client().get('/TestHoax/')
        self.assertTemplateUsed(response, "home2.html")

    def test_models_JawabanUser(self):
        var = JawabanUser.objects.create(Jawaban="select1", Jawaban_ID=1)
        self.assertEquals(var.Jawaban, "select1")
        self.assertEquals(var.Jawaban_ID, 1)

    def test_url_home_test_hoaks(self):
        response = Client().get('/TestHoax/')
        self.assertEquals(response.status_code, 200)

    def test_konten_home_test_hoaks(self):
        response = Client().get('/TestHoax/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Selamat Datang di Test Hoax !", html_kembalian)
        self.assertIn("Ikuti Test", html_kembalian)
    
    def test_url_soal1_test_hoaks(self):
        response = Client().get('/TestHoax/soal/')
        self.assertEquals(response.status_code, 200)
    
    def test_template_soal1(self):
        response = Client().get('/TestHoax/soal/')
        self.assertTemplateUsed(response, "TestHoax/Soal.html")

    def test_konten_soal1(self):
        response = Client().get('/TestHoax/soal/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Perhatikan Gambar Berikut Ini !", html_kembalian)

    def test_url_soal2(self):
        url = reverse("TestHoax:soal2", {'nomor':2, 'skor':20})
        print(url)