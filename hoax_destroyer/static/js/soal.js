const startButton = document.getElementById('start-btn')
const nextButton = document.getElementById('next-btn')
const exitButton = document.getElementById('exit-btn')
const forumButton = document.getElementById('forum-btn')
const numberQuestion = document.getElementById('number')
const questionContainerElement = document.getElementById('question-container')
const questionElement = document.getElementById('question')
const choiceA = document.getElementById("A");
const choiceB = document.getElementById("B");
const choiceC = document.getElementById("C");
const choiceD = document.getElementById("D");
const optionA = document.getElementById("OA");
const optionB = document.getElementById("OB");
const optionC = document.getElementById("OC");
const optionD = document.getElementById("OD");
const answerButtonsElement = document.getElementById('answer-buttons')
const scoreDiv = document.getElementById("scoreContainer");

const questions = [
    {
        question : "Menyemprotkan seluruh tubuh dengan alkohol dapat membunuh virus corona secara efektif. Apakah pernyataan tersebut benar? ",
        choiceA : "Salah, karena cara tersebut dapat membahayakan tubuh pengguna",
        choiceB : "Salah, karena kita tidak perlu menyemprotkan seluruh tubuh, melainkan hanya bagian-bagian tertentu saja",
        choiceC : "Benar, namun perlu menggunakan alkohol dengan kadar 90%",
        choiceD : "Benar, cara tersebut tidak boleh dilakukan setiap hari",
        optionA : "A",
        optionB : "B",
        optionC : "C",
        optionD : "D",
        correct : "A"
    },{
        question : "Thermo Gun, alat untuk mengukur suhu tubuh, ... untuk otak karena menggunakan teknologi ...",
        choiceA : "tidak berbahaya, laser",
        choiceB : "berbahaya, inframerah",
        choiceC : "tidak berbahaya, inframerah",
        choiceD : "berbahaya, laser",
        optionA : "A",
        optionB : "B",
        optionC : "C",
        optionD : "D",
        correct : "C"
    },{
        question : "Berikut ini adalah masker yang TIDAK dianjurkan oleh pemerintah, yakni ...",
        choiceA : "Masker kain 3 lapis",
        choiceB : "Masker bedah",
        choiceC : "Masker N95",
        choiceD : "Masker Scuba",
        optionA : "A",
        optionB : "B",
        optionC : "C",
        optionD : "D",
        correct : "D"
    },{
        question : "Manakah prosedur pemakaian masker yang salah di bawah ini?",
        choiceA : "Buang masker ke tempat sampah, lalu cuci tangan",
        choiceB : "Hindari menyentuh masker saat digunakan",
        choiceC : "Pasang masker untuk menutupi daerah hidung",
        choiceD : "Mencuci tangan sebelum memasang masker",
        optionA : "A",
        optionB : "B",
        optionC : "C",
        optionD : "D",
        correct : "C"
    }
];


const lastQuestion = questions.length - 1;
let currentQuestionIndex;
let score;
let number;

startButton.addEventListener('click', startGame)
nextButton.addEventListener('click', () => {
  currentQuestionIndex++
  setNextQuestion()
})

function startGame() {
  startButton.classList.add('hide')
  exitButton.classList.add('hide')
  forumButton.classList.add('hide')
  numberQuestion.classList.add("hide")
  currentQuestionIndex = 0
  score = 0;
  number = 0;
  questionContainerElement.classList.remove('hide')
  setNextQuestion()
}

function setNextQuestion() {
  nextButton.classList.add('hide');
  number++;
  showQuestion(questions[currentQuestionIndex])
}

function showQuestion(question) {
  answerButtonsElement.classList.remove("hide")
  questionElement.classList.remove("hide")
  numberQuestion.classList.remove("hide")
  scoreDiv.style.display = "none";
  numberQuestion.innerHTML = "<h1>Soal "+ number + "</h1>"
  questionElement.innerText = question.question
    clearStatusClass(choiceA);
    clearStatusClass(choiceB);
    clearStatusClass(choiceC);
    clearStatusClass(choiceD);
    clearStatusClass(optionA);
    clearStatusClass(optionB);
    clearStatusClass(optionC);
    clearStatusClass(optionD);
    choiceA.disabled = false;
    choiceB.disabled = false;
    choiceC.disabled = false;
    choiceD.disabled = false;
    optionA.disabled = false;
    optionB.disabled = false;
    optionC.disabled = false;
    optionD.disabled = false;
    choiceA.innerHTML = question.choiceA;
    choiceB.innerHTML = question.choiceB;
    choiceC.innerHTML = question.choiceC;
    choiceD.innerHTML = question.choiceD;
    optionA.innerHTML = question.optionA;
    optionB.innerHTML = question.optionB;
    optionC.innerHTML = question.optionC;
    optionD.innerHTML = question.optionD;
}

function checkAnswer(answer) {
    if( answer == questions[currentQuestionIndex].correct){
        score++;
        setStatusCorrectClass(answer);
    }
    else {
        setStatusCorrectClass(questions[currentQuestionIndex].correct);
        setStatusWrongClass(answer);
    }

    choiceA.disabled = true;
    choiceB.disabled = true;
    choiceC.disabled = true;
    choiceD.disabled = true;
    optionA.disabled = true;
    optionB.disabled = true;
    optionC.disabled = true;
    optionD.disabled = true;

    if(currentQuestionIndex < lastQuestion){
        nextButton.classList.remove('hide')
    }
    else{
        answerButtonsElement.classList.add("hide")
        questionElement.classList.add("hide")
        numberQuestion.classList.add("hide")
        startButton.innerText = 'Restart'
        startButton.classList.remove('hide')
	exitButton.classList.remove('hide')
	forumButton.classList.remove('hide')
	scoreRender();
    }

}

function setStatusCorrectClass(ans) {
  if(ans == "A") {
     choiceA.classList.add("correct");
     optionA.classList.add("correct");
  }
  else if(ans == "B") {
     choiceB.classList.add("correct");
     optionB.classList.add("correct");
  }
  else if(ans == "C") {
     choiceC.classList.add("correct");
     optionC.classList.add("correct");
  }
  else {
     choiceD.classList.add("correct");
     optionD.classList.add("correct");
  }
}

function setStatusWrongClass(ans) {
  if(ans == "A") {
     choiceA.classList.add("wrong");
     optionA.classList.add("wrong");
  }
  else if(ans == "B") {
     choiceB.classList.add("wrong");
     optionB.classList.add("wrong");
  }
  else if(ans == "C") {
     choiceC.classList.add("wrong");
     optionC.classList.add("wrong");
  }
  else {
     choiceD.classList.add("wrong");
     optionD.classList.add("wrong");
  }
}

function clearStatusClass(element) {
  element.classList.remove('correct')
  element.classList.remove('wrong')
}

function scoreRender(){
    scoreDiv.style.display = "block";
    const scorePerCent = Math.round(100 * score/questions.length);
    scoreDiv.innerHTML = "<p style='text-algin: center;'>Score: "+ scorePerCent +"/100</p>";
}


