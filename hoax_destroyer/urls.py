from django.urls import path

from . import views

app_name = 'hoax_destroyer'

urlpatterns = [
    path('start/', views.start, name='start'),
    path('soal1/', views.soal1, name='soal1'),
    path('hasil1/<int:opsiid>/<int:nilaiid>/', views.hasil1, name='hasil1'),
    path('soal2/<int:nilaiid>/', views.soal2, name='soal2'),
    path('hasil2/<int:opsiid>/<int:nilaiid>/', views.hasil2, name='hasil2'),
    path('soal3/<int:nilaiid>/', views.soal3, name='soal3'),
    path('hasil3/<int:opsiid>/<int:nilaiid>/', views.hasil3, name='hasil3'),
    path('soal4/<int:nilaiid>/', views.soal4, name='soal4'),
    path('hasil4/<int:opsiid>/<int:nilaiid>/', views.hasil4, name='hasil4'),
    path('soal5/<int:nilaiid>/', views.soal5, name='soal5'),
    path('hasil5/<int:opsiid>/<int:nilaiid>/', views.hasil5, name='hasil5'),
    path('score/<int:nilaiid>/', views.score, name='score'),
    path('saranPertanyaan/', views.saranPertanyaan, name='saranPertanyaan'),
    path('saranPertanyaan/saveSaran', views.saveSaran),
]

