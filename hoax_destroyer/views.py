from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import *
from .forms import *

def start(request):
	Pertanyaan.objects.all().delete()
	Opsi.objects.all().delete()
	Nilai.objects.all().delete()
	Soal1 = Pertanyaan.objects.create(nomor=1,pertanyaan="Menyemprotkan seluruh tubuh dengan alkohol dapat membunuh virus corona secara efektif. Apakah pernyataan tersebut benar? ")
	Soal2 = Pertanyaan.objects.create(nomor=2,pertanyaan="Thermo Gun, alat untuk mengukur suhu tubuh, ... untuk otak karena menggunakan teknologi ...")
	Soal3 = Pertanyaan.objects.create(nomor=3,pertanyaan="Berikut ini adalah masker yang TIDAK dianjurkan oleh pemerintah, yakni ...")
	Soal4 = Pertanyaan.objects.create(nomor=4,pertanyaan="Manakah prosedur pemakaian masker yang salah di bawah ini?")
	Soal5 = Pertanyaan.objects.create(nomor=5,pertanyaan="Cara berikut ini dianggap ampuh untuk mencegah terkena virus corona, kecuali...")
	Soal1.save()
	Soal2.save()
	Soal3.save()
	Soal4.save()
	Soal5.save()

	opsia1 =	Opsi.objects.create(kode="a1",pilihan="Salah, karena cara tersebut dapat membahayakan tubuh pengguna",correct=True)
	opsib1 = Opsi.objects.create(kode="b1",pilihan="Salah, karena kita tidak perlu menyemprotkan seluruh tubuh, melainkan hanya bagian-bagian tertentu saja",correct=False)
	opsic1 = Opsi.objects.create(kode="c1",pilihan="Benar, namun perlu menggunakan alkohol dengan kadar 90%",correct=False)
	opsid1 = Opsi.objects.create(kode="d1",pilihan="Benar, cara tersebut tidak boleh dilakukan setiap hari",correct=False)
	opsia1.save()
	opsib1.save()
	opsic1.save()
	opsid1.save()

	opsia2 = Opsi.objects.create(kode="a2",pilihan="tidak berbahaya, laser",correct=False)
	opsib2 = Opsi.objects.create(kode="b2",pilihan="berbahaya, inframerah",correct=False)
	opsic2 = Opsi.objects.create(kode="c2",pilihan="tidak berbahaya, inframerah",correct=True)
	opsid2 = Opsi.objects.create(kode="d2",pilihan="berbahaya, laser",correct=False)
	opsia2.save()
	opsib2.save()
	opsic2.save()
	opsid2.save()

	opsia3 = Opsi.objects.create(kode="a3",pilihan="Masker kain 3 lapis",correct=False)
	opsib3 = Opsi.objects.create(kode="b3",pilihan="Masker bedah",correct=False)
	opsic3 = Opsi.objects.create(kode="c3",pilihan="Masker N95",correct=False)
	opsid3 = Opsi.objects.create(kode="d3",pilihan="Masker Scuba",correct=True)
	opsia3.save()
	opsib3.save()
	opsic3.save()
	opsid3.save()

	opsia4 = Opsi.objects.create(kode="a4",pilihan="Buang masker ke tempat sampah, lalu cuci tangan",correct=False)
	opsib4 = Opsi.objects.create(kode="b4",pilihan="Hindari menyentuh masker saat digunakan",correct=False)
	opsic4 = Opsi.objects.create(kode="c4",pilihan="Pasang masker untuk menutupi daerah hidung",correct=True)
	opsid4 = Opsi.objects.create(kode="d4",pilihan="Mencuci tangan sebelum memasang masker",correct=False)
	opsia4.save()
	opsib4.save()
	opsic4.save()
	opsid4.save()

	opsia5 = Opsi.objects.create(kode="a5",pilihan="Mencuci tangan menggunakan sabun selama 20 detik",correct=False)
	opsib5 = Opsi.objects.create(kode="b5",pilihan="Mandi dengan air bersuhu tinggi setelah berpergian",correct=True)
	opsic5 = Opsi.objects.create(kode="c5",pilihan="Menggunakan masker sesuai arahan pemerintah",correct=False)
	opsid5 = Opsi.objects.create(kode="d5",pilihan="Menjaga jarak ketika berbicara dan sebisa mungkin menghindari kerumunan",correct=False)
	opsia5.save()
	opsib5.save()
	opsic5.save()
	opsid5.save()

	nilai = Nilai.objects.create(nilai=0)

	return render(request,'soalHD/start.html')

def soal1(request):
    soal = Pertanyaan.objects.get(nomor=1)
    nilai = Nilai.objects.create(nilai=0)
    a=Opsi.objects.get(kode="a1")
    b=Opsi.objects.get(kode="b1")
    c=Opsi.objects.get(kode="c1")
    d=Opsi.objects.get(kode="d1")
    context = {
	    'soal':soal,
        'nilai' : nilai,
	    'a' : a,
	    'b' : b,
	    'c' : c,
	    'd' : d,
    } 
    return render(request,'soalHD/soal1.html',context)

def hasil1(request,opsiid,nilaiid):
    opsisoal = Opsi.objects.get(id=opsiid)
    soal = Pertanyaan.objects.get(nomor=1)
    a=Opsi.objects.get(kode="a1")
    b=Opsi.objects.get(kode="b1")
    c=Opsi.objects.get(kode="c1")
    d=Opsi.objects.get(kode="d1")
    nilai = Nilai.objects.get(id=nilaiid)
    if opsisoal.correct == True:
        nilai.nilai = nilai.nilai+20
        nilai.save()
    context = {
        'soal':soal,
        'nilai' : nilai,
	    'a' : a,
        'b' : b,
        'c' : c,
        'd' : d,
    } 
    return render(request,'soalHD/hasil1.html',context)

def soal2(request,nilaiid):
    soal = Pertanyaan.objects.get(nomor=2)
    nilai = Nilai.objects.get(id=nilaiid)
    a=Opsi.objects.get(kode="a2")
    b=Opsi.objects.get(kode="b2")
    c=Opsi.objects.get(kode="c2")
    d=Opsi.objects.get(kode="d2")
    context = {
        'soal':soal,
        'nilai' : nilai,
        'a' : a,
        'b' : b,
        'c' : c,
        'd' : d,
    } 
    return render(request,'soalHD/soal2.html',context)

def hasil2(request,opsiid,nilaiid):
    opsisoal = Opsi.objects.get(id=opsiid)
    nilai = Nilai.objects.get(id=nilaiid)
    soal = Pertanyaan.objects.get(nomor=2)
    a=Opsi.objects.get(kode="a2")
    b=Opsi.objects.get(kode="b2")
    c=Opsi.objects.get(kode="c2")
    d=Opsi.objects.get(kode="d2")
    if opsisoal.correct == True:
        nilai.nilai = nilai.nilai+20
        nilai.save()
    context = {
        'soal':soal,
        'nilai' : nilai,
        'a' : a,
        'b' : b,
        'c' : c,
        'd' : d,
    } 
    return render(request,'soalHD/hasil2.html',context)

def soal3(request,nilaiid):
    soal = Pertanyaan.objects.get(nomor=3)
    nilai = Nilai.objects.get(id=nilaiid)
    a=Opsi.objects.get(kode="a3")
    b=Opsi.objects.get(kode="b3")
    c=Opsi.objects.get(kode="c3")
    d=Opsi.objects.get(kode="d3")
    context = {
        'soal':soal,
        'nilai' : nilai,
        'a' : a,
        'b' : b,
        'c' : c,
        'd' : d,
    }
    return render(request,'soalHD/soal3.html',context)

def hasil3(request,opsiid,nilaiid):
    opsisoal = Opsi.objects.get(id=opsiid)
    nilai = Nilai.objects.get(id=nilaiid)
    soal = Pertanyaan.objects.get(nomor=3)
    a=Opsi.objects.get(kode="a3")
    b=Opsi.objects.get(kode="b3")
    c=Opsi.objects.get(kode="c3")
    d=Opsi.objects.get(kode="d3")
    if opsisoal.correct == True:
        nilai.nilai = nilai.nilai+20
        nilai.save()
    context = {
        'soal':soal,
        'nilai' : nilai,
        'a' : a,
        'b' : b,
        'c' : c,
        'd' : d,
    }
    return render(request,'soalHD/hasil3.html',context)

def soal4(request,nilaiid):
    soal = Pertanyaan.objects.get(nomor=4)
    nilai = Nilai.objects.get(id=nilaiid)
    a=Opsi.objects.get(kode="a4")
    b=Opsi.objects.get(kode="b4")
    c=Opsi.objects.get(kode="c4")
    d=Opsi.objects.get(kode="d4")
    context = {
        'soal':soal,
        'nilai' : nilai,
        'a' : a,
        'b' : b,
        'c' : c,
        'd' : d,
    }
    return render(request,'soalHD/soal4.html',context)

def hasil4(request,opsiid,nilaiid):
    opsisoal = Opsi.objects.get(id=opsiid)
    nilai = Nilai.objects.get(id=nilaiid)
    soal = Pertanyaan.objects.get(nomor=4)
    a=Opsi.objects.get(kode="a4")
    b=Opsi.objects.get(kode="b4")
    c=Opsi.objects.get(kode="c4")
    d=Opsi.objects.get(kode="d4")
    if opsisoal.correct == True:
        nilai.nilai = nilai.nilai+20
        nilai.save()
    context = {
        'soal':soal,
        'nilai' : nilai,
        'a' : a,
        'b' : b,
        'c' : c,
        'd' : d,
    }
    return render(request,'soalHD/hasil4.html',context)

def soal5(request,nilaiid):
    soal = Pertanyaan.objects.get(nomor=5)
    nilai = Nilai.objects.get(id=nilaiid)
    a=Opsi.objects.get(kode="a5")
    b=Opsi.objects.get(kode="b5")
    c=Opsi.objects.get(kode="c5")
    d=Opsi.objects.get(kode="d5")
    context = {
        'soal':soal,
        'nilai' : nilai,
        'a' : a,
        'b' : b,
        'c' : c,
        'd' : d,
    }
    return render(request,'soalHD/soal5.html',context)

def hasil5(request,opsiid,nilaiid):
    opsisoal = Opsi.objects.get(id=opsiid)
    nilai = Nilai.objects.get(id=nilaiid)
    soal = Pertanyaan.objects.get(nomor=5)
    a=Opsi.objects.get(kode="a5")
    b=Opsi.objects.get(kode="b5")
    c=Opsi.objects.get(kode="c5")
    d=Opsi.objects.get(kode="d5")
    if opsisoal.correct == True:
        nilai.nilai = nilai.nilai+20
        nilai.save()
    context = {
        'soal': soal,
        'nilai' : nilai,
        'a' : a,
        'b' : b,
        'c' : c,
        'd' : d,
    }
    return render(request,'soalHD/hasil5.html',context)

def score(request,nilaiid):
    nilai = Nilai.objects.get(id=nilaiid)
    context={
        'nilai': nilai.nilai,
    }
    return render(request,'soalHD/score.html',context)

def saveSaran(request):
    form = detail_pertanyaan(request.POST or None)
    response = {}
    if (form.is_valid and request.method == 'POST'):
        response['pertanyaan'] = request.POST['pertanyaan']
        response['opsi1'] = request.POST['opsi1']
        response['opsi2'] = request.POST['opsi2']
        response['opsi3'] = request.POST['opsi3']
        response['opsi4'] = request.POST['opsi4']
        saran = Saran(pertanyaan=response['pertanyaan'], opsi1=response['opsi1'], opsi2=response['opsi2'], opsi3=response['opsi3'], opsi4=response['opsi4'])
        saran.save()
        return HttpResponseRedirect('/hoax_destroyer/saranPertanyaan')
    else:
        return HttpResponseRedirect('/')

def saranPertanyaan(request):
    form = detail_pertanyaan()
    context={
        'form': form
    }
    return render(request,'soalHD/saran.html',context)

