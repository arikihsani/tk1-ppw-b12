from django.apps import AppConfig


class HoaxDestroyerConfig(AppConfig):
    name = 'hoax_destroyer'
