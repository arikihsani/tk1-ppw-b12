from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Pertanyaan)
admin.site.register(Opsi)
admin.site.register(Nilai)
admin.site.register(Saran)
