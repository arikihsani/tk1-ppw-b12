from django.db import models

# Create your models here.

class Pertanyaan(models.Model):
    nomor = models.IntegerField(default=1)
    pertanyaan = models.CharField(max_length=300)

class Opsi(models.Model):
    kode = models.CharField(max_length=20)
    pilihan = models.CharField(max_length=300)
    correct = models.BooleanField(default=False)

class Nilai(models.Model):
    nilai = models.IntegerField(default=0)

class Saran(models.Model):
    pertanyaan = models.CharField(max_length=300)
    opsi1 = models.CharField(max_length=300)
    opsi2 = models.CharField(max_length=300)
    opsi3 = models.CharField(max_length=300)
    opsi4 = models.CharField(max_length=300)
