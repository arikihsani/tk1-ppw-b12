from django import forms
from .models import *

class detail_pertanyaan(forms.ModelForm):
    class Meta:
        model = Saran
        fields = ['pertanyaan', 'opsi1', 'opsi2', 'opsi3', 'opsi4']
    input_attrs1 = {
        'type' : 'text',
        'placeholder' : 'pertanyaan',
        'style' : 'background-color: #E8EEF2'
    }
    input_attrs2 = {
        'type' : 'text',
        'placeholder' : 'jawaban benar',
        'style' : 'background-color: #E8EEF2'
    }
    input_attrs3 = {
        'type' : 'text',
        'placeholder' : 'Jawaban salah',
        'style' : 'background-color: #E8EEF2'
    }
    pertanyaan = forms.CharField(label='Pertanyaan', max_length=100, widget=forms.Textarea(attrs=input_attrs1))
    opsi1 = forms.CharField(label='Opsi 1', max_length=100, widget=forms.TextInput(attrs=input_attrs2))
    opsi2 = forms.CharField(label='Opsi 2', max_length=100, widget=forms.TextInput(attrs=input_attrs3))
    opsi3 = forms.CharField(label='Opsi 3', max_length=100, widget=forms.TextInput(attrs=input_attrs3))
    opsi4 = forms.CharField(label='Opsi 4', max_length=100, widget=forms.TextInput(attrs=input_attrs3))


