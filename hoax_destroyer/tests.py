from django.test import TestCase, Client		
from django.urls import resolve
from .models import *
from .views import *
from .forms import *

class TestHoaxDestroyer(TestCase):

    def test_start_ada_enggak(self):
    	Pertanyaan.objects.all().delete()
    	Opsi.objects.all().delete()
    	response = Client().get('/hoax_destroyer/start/')
    	self.assertEqual(response.status_code,200)

    def test_soal1_bisa_enggak(self):
    	a = Opsi.objects.create(kode="a1",pilihan="-",correct=False)
    	b = Opsi.objects.create(kode="b1",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c1",pilihan="-",correct=True)
    	d = Opsi.objects.create(kode="d1",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=1,pertanyaan="-")
    	response = Client().get('/hoax_destroyer/soal1/')
    	self.assertEqual(response.status_code,200)

    def test_hasil1_bisa_enggak(self):
    	nilai = Nilai.objects.create(nilai=0)
    	a = Opsi.objects.create(kode="a1",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b1",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c1",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d1",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=1,pertanyaan="-")
    	response = Client().get('/hoax_destroyer/hasil1/2/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal2_bisa_enggak(self):
    	nilai = Nilai.objects.create(nilai=0)
    	a = Opsi.objects.create(kode="a2",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b2",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c2",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d2",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=2,pertanyaan="-")
    	response = Client().get('/hoax_destroyer/soal2/1/')
    	self.assertEqual(response.status_code,200)

    def test_hasil2_bisa_enggak(self):
    	nilai = Nilai.objects.create(nilai=0)
    	a = Opsi.objects.create(kode="a2",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b2",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c2",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d2",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=2,pertanyaan="-")
    	response = Client().get('/hoax_destroyer/hasil2/2/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal3_bisa_enggak(self):
    	nilai = Nilai.objects.create(nilai=0)
    	a = Opsi.objects.create(kode="a3",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b3",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c3",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d3",pilihan="-",correct=False)
    	Soal1=Pertanyaan.objects.create(nomor=3,pertanyaan="-")
    	response = Client().get('/hoax_destroyer/soal3/1/')
    	self.assertEqual(response.status_code,200)

    def test_hasil3_bisa_enggak(self):
    	nilai = Nilai.objects.create(nilai=0)
    	a = Opsi.objects.create(kode="a3",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b3",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c3",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d3",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=3,pertanyaan="-")
    	response = Client().get('/hoax_destroyer/hasil3/2/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal4_bisa_enggak(self):
    	nilai = Nilai.objects.create(nilai=0)
    	a = Opsi.objects.create(kode="a4",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b4",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c4",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d4",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=4,pertanyaan="-")
    	response = Client().get('/hoax_destroyer/soal4/1/')
    	self.assertEqual(response.status_code,200)

    def test_hasil4_bisa_enggak(self):
    	nilai = Nilai.objects.create(nilai=0)
    	a = Opsi.objects.create(kode="a4",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b4",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c4",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d4",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=4,pertanyaan="-")
    	response = Client().get('/hoax_destroyer/hasil4/2/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal5_bisa_enggak(self):
    	nilai = Nilai.objects.create(nilai=0)
    	a = Opsi.objects.create(kode="a5",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b5",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c5",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d5",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=5,pertanyaan="-")
    	response = Client().get('/hoax_destroyer/soal5/1/')
    	self.assertEqual(response.status_code,200)

    def test_hasil5_bisa_enggak(self):
    	nilai = Nilai.objects.create(nilai=0)
    	a = Opsi.objects.create(kode="a5",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b5",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c5",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d5",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=5,pertanyaan="-")
    	response = Client().get('/hoax_destroyer/hasil5/2/1/')
    	self.assertEqual(response.status_code,200)

    def test_score_bisa_enggak(self):
    	nilai = Nilai.objects.create(nilai=0)
    	response = Client().get('/hoax_destroyer/score/1/')
    	self.assertEqual(response.status_code,200)

    def test_saranPertanyaan_bisa_enggak(self):
    	response = Client().get('/hoax_destroyer/saranPertanyaan/')
    	self.assertEqual(response.status_code,200)

    def test_fungsi_start(self):
        found = resolve('/hoax_destroyer/start/')
        self.assertEqual(found.func,start)

    def test_fungsi_soal1(self):
    	a = Opsi.objects.create(kode="a1",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b1",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c1",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d1",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=1,pertanyaan="-")
    	response = resolve('/hoax_destroyer/soal1/')
    	self.assertEqual(response.func,soal1)

    def test_fungsi_hasil1(self):
    	a = Opsi.objects.create(kode="a1",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b1",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c1",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d1",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=1,pertanyaan="-")
    	response = resolve('/hoax_destroyer/hasil1/2/1/')
    	self.assertEqual(response.func,hasil1)

    def test_fungsi_soal2(self):
    	nilai = Nilai.objects.create(nilai=0)
    	a = Opsi.objects.create(kode="a2",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b2",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c2",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d2",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=2,pertanyaan="-")
    	response = resolve('/hoax_destroyer/soal2/1/')
    	self.assertEqual(response.func,soal2)

    def test_fungsi_hasil2(self):
    	a = Opsi.objects.create(kode="a2",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b2",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c2",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d2",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=2,pertanyaan="-")
    	response = resolve('/hoax_destroyer/hasil2/2/1/')
    	self.assertEqual(response.func,hasil2)

    def test_fungsi_soal3(self):
    	nilai = Nilai.objects.create(nilai=0)
    	a = Opsi.objects.create(kode="a3",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b3",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c3",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d3",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=3,pertanyaan="-")
    	response = resolve('/hoax_destroyer/soal3/1/')
    	self.assertEqual(response.func,soal3)

    def test_fungsi_hasil3(self):
    	a = Opsi.objects.create(kode="a3",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b3",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c3",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d3",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=3,pertanyaan="-")
    	response = resolve('/hoax_destroyer/hasil3/2/1/')
    	self.assertEqual(response.func,hasil3)

    def test_fungsi_soal4(self):
    	nilai = Nilai.objects.create(nilai=0)
    	a = Opsi.objects.create(kode="a4",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b4",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c4",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d4",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=4,pertanyaan="-")
    	response = resolve('/hoax_destroyer/soal4/1/')
    	self.assertEqual(response.func,soal4)

    def test_fungsi_hasil4(self):
    	a = Opsi.objects.create(kode="a4",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b4",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c4",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d4",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=4,pertanyaan="-")
    	response = resolve('/hoax_destroyer/hasil4/2/1/')
    	self.assertEqual(response.func,hasil4)

    def test_fungsi_soal5(self):
    	nilai = Nilai.objects.create(nilai=0)
    	a = Opsi.objects.create(kode="a5",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b5",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c5",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d5",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=5,pertanyaan="-")
    	response = resolve('/hoax_destroyer/soal5/1/')
    	self.assertEqual(response.func,soal5)

    def test_fungsi_hasil5(self):
    	a = Opsi.objects.create(kode="a5",pilihan="-",correct=True)
    	b = Opsi.objects.create(kode="b5",pilihan="-",correct=False)
    	c = Opsi.objects.create(kode="c5",pilihan="-",correct=False)
    	d = Opsi.objects.create(kode="d5",pilihan="-",correct=False)
    	soal = Pertanyaan.objects.create(nomor=5,pertanyaan="-")
    	response = resolve('/hoax_destroyer/hasil5/2/1/')
    	self.assertEqual(response.func,hasil5)

    def test_fungsi_score(self):
    	nilai = Nilai.objects.create(nilai=0)
    	response = resolve('/hoax_destroyer/score/1/')
    	self.assertEqual(response.func,score)

    def test_fungsi_saranPertanyaan(self):
    	form = detail_pertanyaan()
    	response = resolve('/hoax_destroyer/saranPertanyaan/')
    	self.assertEqual(response.func,saranPertanyaan)

    def test_model_benar(self):
        soal = Pertanyaan.objects.create(nomor=6,pertanyaan="-")
        a = Opsi.objects.create(kode="a6",pilihan="#",correct=True)
        b = Opsi.objects.create(kode="b6",pilihan="#",correct=False)
        c = Opsi.objects.create(kode="c6",pilihan="#",correct=False)
        d = Opsi.objects.create(kode="d6",pilihan="#",correct=False)
        nilai = Nilai.objects.create(nilai=0)
        saran = Saran.objects.create(pertanyaan="-",opsi1="-",opsi2="-",opsi3="-",opsi4="-")
        jumlahSoal = Pertanyaan.objects.all().count()
        jumlahOpsi = Opsi.objects.all().count()
        jumlahNilai = Nilai.objects.all().count()
        jumlahSaran = Saran.objects.all().count()
        self.assertEqual(jumlahSoal,1)
        self.assertEqual(jumlahNilai,1)
        self.assertEqual(jumlahOpsi,4)
        self.assertEqual(jumlahSaran,1)
