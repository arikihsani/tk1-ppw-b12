from django.urls import path
from .views import *
from . import views
from django.contrib.auth import views as auth_views
from .forms import AccountLoginForm

app_name = 'userAuth'

urlpatterns = [
    path('signup/', Signup.as_view(), name='signup'),
    path('login/', auth_views.LoginView.as_view(authentication_form=AccountLoginForm), name='login'),
]
