# Generated by Django 3.1.2 on 2020-11-12 04:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='replycomment',
            old_name='comment',
            new_name='komen',
        ),
    ]
