from django import forms
from .models import Post, Comment, ReplyComment, Color
from forum.models import KATEGORI

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ["judul", "kategori", "isi"]
    
    error_messages = {
        'required' : 'Informasi Wajib Diisi'
    }
    
    input_attrs_judul = {
        'type' : 'text',
        'placeholder' : 'Judul Diskusi',
        'class' : 'form-control',
        'autocomplete' : 'off'
    }
    
    input_attrs_kategori = {
        'type' : 'text',
        'placeholder' : 'Kategori Diskusi',
        'class' : 'form-control',
        'autocomplete' : 'off'
    }
    
    input_attrs_isi = {
        'type' : 'text',
        'placeholder' : 'Isi Diskusi',
        'class' : 'form-control',
        'autocomplete' : 'off'
    }
    
    judul = forms.CharField(label='', required=True, max_length=150, widget=forms.TextInput(attrs=input_attrs_judul))
    kategori = forms.ChoiceField(label='', choices=KATEGORI, widget=forms.Select(attrs=input_attrs_kategori))
    isi = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=input_attrs_isi))

    
class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ["isi"]
        exclude = ('post', 'penulis', 'tanggalPost',)
    
    error_messages = {
        'required' : 'Informasi Wajib Diisi'
    }
    
    input_attrs_isi = {
        'type' : 'text',
        'placeholder' : 'Komentar',
        'class' : 'form-control form-comment',
        'autocomplete' : 'off',
        'rows' : 2
    }
    
    isi = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=input_attrs_isi))

    
class ReplyCommentForm(forms.ModelForm):
    class Meta:
        model = ReplyComment
        fields = ["isi"]
        exclude = ('comment', 'penulis', 'tanggalPost',)
    
    error_messages = {
        'required' : 'Informasi Wajib Diisi'
    }
    
    input_attrs_isi = {
        'type' : 'text',
        'placeholder' : 'Balas Komentar',
        'class' : 'form-control form-reply',
        'autocomplete' : 'off',
        'autofocus' : 'autofocus'
    }
    
    isi = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=input_attrs_isi))
    
class ColorForm(forms.ModelForm):
    class Meta:
        model = Color
        fields = ["color1", "color2", "color3"]
        exclude = ('user',)
    
    error_messages = {
        'required' : 'Informasi Wajib Diisi'
    }
    
    input_attrs_color = {
        'type' : 'number',
        'placeholder' : 'Angka 0 sampai 255',
        'class' : 'form-control form-rgb',
        'autocomplete' : 'off'
    }
    
    color1 = forms.IntegerField(label='Red', required=True, widget=forms.TextInput(attrs=input_attrs_color))
    color2 = forms.IntegerField(label='Green', required=True, widget=forms.TextInput(attrs=input_attrs_color))
    color3 = forms.IntegerField(label='Blue', required=True, widget=forms.TextInput(attrs=input_attrs_color))
    