from django.shortcuts import get_object_or_404, redirect, render, reverse
from django.http import HttpResponseForbidden, HttpResponseNotFound, HttpResponseRedirect
from .forms import PostForm, CommentForm, ReplyCommentForm, ColorForm
from .models import Post, Comment, ReplyComment, Color
import random
from django.contrib.auth.models import User

def forum(request, category=None):
    #warna
    if request.user.is_authenticated:
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
    else:
        warnaUser = None
    semuaWarna = Color.objects.all()
    
    if category == 'user' and request.user.is_authenticated:
        diskusi = Post.objects.filter(penulis = request.user).order_by('-tanggalPost')
    elif category != 'semuaDiskusi' and category:
        diskusi = Post.objects.filter(kategori = category).order_by('-tanggalPost')
    else:
        diskusi = Post.objects.all().order_by('-tanggalPost')
        
    if (category == 'user' and not request.user.is_authenticated) or (category != 'semuaDiskusi' and category != 'umum' and category != 'info' and category != 'hd' and category != 'hs' and category != 'th' and category != 'tp' and category != 'user' and category != None):
        return HttpResponseNotFound()
    if category:
        context = {'category' : category, 'diskusi' : diskusi, 'semuaWarna' : semuaWarna, 'warnaUser' : warnaUser}
        return render(request, 'forum/forum.html', context)
        
    context = {'category' : 'semuaDiskusi', 'diskusi' : diskusi, 'semuaWarna' : semuaWarna, 'warnaUser' : warnaUser}
    return render(request, 'forum/forum.html', context)


def editPost(request, pk=None):
    
    if request.user.is_authenticated:
    
        if pk:
            post = get_object_or_404(Post, pk=pk)
            if post.penulis != request.user:
                return HttpResponseForbidden()
        else:
            post = Post(penulis = request.user)

        form = PostForm(request.POST or None, instance = post)
        if request.POST and form.is_valid():
            form.save()
            
            warna = Color.objects.filter(user = request.user)
            if not warna:
                warna = Color(
                    color1 = random.randint(0, 255),
                    color2 = random.randint(0, 255),
                    color3 = random.randint(0, 255),
                    user = request.user
                )
                warna.save()

            return redirect('forum:readPost', post.pk)
        
        #warna
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
        return render(request, 'forum/editPost.html', {
            'form': form,
            'warnaUser' : warnaUser
        })
    
    return render(request, 'forum/notLoggedIn.html', {'message':'"Mulai Diskusi"'})
   
   
def readPost(request, pk):
    diskusi = get_object_or_404(Post, pk = pk)
    komen = Comment.objects.filter(post = diskusi)
    reply = ReplyComment.objects.all()
        
    #pick random Posts
    if request.user.is_authenticated:
        postWithoutUser = Post.objects.exclude(penulis = request.user).exclude(kategori = diskusi.kategori).order_by('-tanggalPost')
    else:
        postWithoutUser = Post.objects.exclude(kategori = diskusi.kategori).order_by('-tanggalPost')
    if len(postWithoutUser) > 3:
        random_pk = random.randint(0, len(postWithoutUser)-3)
    else:
        random_pk = 0
    randomDiskusi = postWithoutUser[random_pk:random_pk+3]
        
    #pick latest posts in category
    if request.user.is_authenticated:
        postWithoutUser = Post.objects.filter(kategori = diskusi.kategori).exclude(penulis = request.user).exclude(judul = diskusi.judul).order_by('-tanggalPost')
    else:
        postWithoutUser = Post.objects.filter(kategori = diskusi.kategori).exclude(judul = diskusi.judul).order_by('-tanggalPost')
    diskusiLatestInCategory = postWithoutUser[0:3]
        
    #Form Comment
    if request.user.is_authenticated:
        komenForForm = Comment(penulis = request.user, post = diskusi)
        form = CommentForm(request.POST or None, instance = komenForForm)
        if request.POST and form.is_valid():
            form.save()
            return HttpResponseRedirect(request.path_info)
    else:
        form = None
        
    #warna
    if request.user.is_authenticated:
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
    else:
        warnaUser = None
    semuaWarna = Color.objects.all()
    return render(request, 'forum/readPost.html', {
        'diskusi' : diskusi, 
        'komen' : komen, 
        'reply' : reply, 
        'form' : form, 
        'randomDiskusi' : randomDiskusi, 
        'diskusiLatestInCategory' : diskusiLatestInCategory, 
        'semuaWarna' : semuaWarna, 
        'warnaUser' : warnaUser
        })
    

def editComment(request, pk):
    if request.user.is_authenticated:
    
        komen = get_object_or_404(Comment, pk=pk)
        if komen.penulis != request.user:
            return HttpResponseForbidden()
                
        form = CommentForm(request.POST or None, instance = komen)
        if request.POST and form.is_valid():
            form.save()
            return redirect('forum:readPost', komen.post.pk)
        
        type = 'edit'
        
        #warna
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
        semuaWarna = Color.objects.all()
        return render(request, 'forum/editComment.html', {
            'form': form,
            'diskusi' : komen.post,
            'komen' : komen,
            'type' : type,
            'semuaWarna' : semuaWarna,
            'warnaUser' : warnaUser
        })
    return render(request, 'forum/notLoggedIn.html', {'message':'buat atau ubah komentar'})
    

def editReply(request, pkC = None, pkR = None):
    if request.user.is_authenticated:
        target = None
        
        if not pkC and pkR:
            target = get_object_or_404(ReplyComment, pk=pkR)
            komen = target.komen
        else:
            komen = get_object_or_404(Comment, pk=pkC)
    
        if pkC and pkR:
            reply = get_object_or_404(ReplyComment, pk=pkR)
            target = reply
            type = 'edit'
            if reply.penulis != request.user:
                return HttpResponseForbidden()
        elif not pkC and pkR:
            reply = ReplyComment(penulis = request.user, komen = komen, targetBalasan = target, isi = "@"+target.penulis.username+" ")
            type = 'add'
        else:
            reply = ReplyComment(penulis = request.user, komen = komen)
            type = 'add'

        form = ReplyCommentForm(request.POST or None, instance = reply)
        if request.POST and form.is_valid():
            form.save()
            return redirect('forum:readPost', komen.post.pk)
        
        #targetBalasan
        if target != None:
            listTarget = [target]
            while target.targetBalasan != None:
                target = target.targetBalasan
                listTarget.append(target)
            listTarget.reverse()
        else:
            listTarget = None
        
        #warna
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
        semuaWarna = Color.objects.all()
        
        context = {
            'form': form,
            'komen' : komen,
            'listTarget' : listTarget,
            'type' : type,
            'semuaWarna' : semuaWarna,
            'warnaUser' : warnaUser
        }
        return render(request, 'forum/editComment.html', context)
    
    return render(request, 'forum/notLoggedIn.html', {'message':'buat atau ubah balasan'})


def profil(request, username):
    if request.user.is_authenticated:
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
    else:
        warnaUser = None
        
    #check if user exist by username
    filterUser = User.objects.filter(username = username)
    
    if filterUser:
        user = User.objects.get(username = username)
        #check if user aldery have color or not
        filter = Color.objects.filter(user = user)
        
        diskusi = Post.objects.filter(penulis = user).order_by('-tanggalPost')
        semuaWarna = Color.objects.all()
        
        if filter:
            warna = Color.objects.get(user = user)
        
        if request.user == user:
            if not filter:
                warna = Color(user = user)
                
            form = ColorForm(request.POST or None, instance = warna)
            if request.POST and form.is_valid():
                form.save()

                return HttpResponseRedirect(request.path_info)
            
            return render(request, 'forum/profil.html', {'userInProfil' : user, 'warna' : warna, 'form' : form, 'diskusi' : diskusi, 'semuaWarna' : semuaWarna, 'warnaUser' : warnaUser})
            
        if not filter:
            warna = None
        
        return render(request, 'forum/profil.html', {'userInProfil' : user, 'warna' : warna, 'diskusi' : diskusi, 'semuaWarna' : semuaWarna, 'warnaUser' : warnaUser})
    else:
        return HttpResponseNotFound()
        

def warnaRandom(request, username):
    if request.user.is_authenticated:
        if username == request.user.username:
            warna = Color.objects.filter(user = request.user)
            if not warna:
                warna = Color(
                    color1 = random.randint(0, 255),
                    color2 = random.randint(0, 255),
                    color3 = random.randint(0, 255),
                    user = request.user
                )
                warna.save()
            else:
                warna = Color.objects.get(user = request.user)
                warna.color1 = random.randint(0, 255)
                warna.color2 = random.randint(0, 255)
                warna.color3 = random.randint(0, 255)
                warna.save()

            return redirect('forum:profil', username)
        else:
            return HttpResponseForbidden()
    else:
        return render(request, 'forum/notLoggedIn.html', {'message':'"Warna Random"'})
    
    
def confirmDeletePost(request, pk):
    if request.user.is_authenticated:
        post = get_object_or_404(Post, pk=pk)
        if post.penulis != request.user:
            return HttpResponseForbidden()
        
        #warna
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
        semuaWarna = Color.objects.all()
        return render(request, 'forum/confirmDelete.html', {'diskusi':post, 'semuaWarna' : semuaWarna, 'warnaUser' : warnaUser})
    
    return render(request, 'forum/notLoggedIn.html', {'message':'hapus diskusi'})


def deletePost(request, pk):
    if request.user.is_authenticated:
        post = Post.objects.filter(pk=pk)
        if post[:1].get().penulis != request.user:
            return HttpResponseForbidden()
        post.delete()
        return redirect('forum:forum1')
    
    return render(request, 'forum/notLoggedIn.html', {'message':'hapus diskusi'})
    
    
def confirmDeleteComment(request, pk):
    if request.user.is_authenticated:
        comment = get_object_or_404(Comment, pk=pk)
        if comment.penulis != request.user:
            return HttpResponseForbidden()
        
        #warna
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
        semuaWarna = Color.objects.all()
        return render(request, 'forum/confirmDelete.html', {'komen':comment, 'semuaWarna' : semuaWarna, 'warnaUser' : warnaUser})
    
    return render(request, 'forum/notLoggedIn.html', {'message':'hapus komentar'})


def deleteComment(request, pk):
    if request.user.is_authenticated:
        komen = Comment.objects.filter(pk=pk)
        post = komen[:1].get().post
        if komen[:1].get().penulis != request.user:
            return HttpResponseForbidden()
        komen.delete()
        return redirect('forum:readPost', post.pk)
    
    return render(request, 'forum/notLoggedIn.html', {'message':'hapus komentar'})
    
    
def confirmDeleteReply(request, pk):
    if request.user.is_authenticated:
        
        target = get_object_or_404(ReplyComment, pk=pk)
        reply = target
        komen = reply.komen
        if target.penulis != request.user:
            return HttpResponseForbidden()
        
        #targetBalasan
        if target != None:
            listTarget = [target]
            while target.targetBalasan != None:
                target = target.targetBalasan
                listTarget.append(target)
            listTarget.reverse()
        else:
            listTarget = None
        
        #warna
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
        semuaWarna = Color.objects.all()
        return render(request, 'forum/confirmDelete.html', {'reply':reply, 'komen':komen, 'listTarget':listTarget, 'semuaWarna' : semuaWarna, 'warnaUser' : warnaUser})
    
    return render(request, 'forum/notLoggedIn.html', {'message':'hapus balasan'})

def deleteReply(request, pk):
    if request.user.is_authenticated:
        reply = ReplyComment.objects.filter(pk=pk)
        post = reply[:1].get().komen.post
        if reply[:1].get().penulis != request.user:
            return HttpResponseForbidden()
        reply.delete()
        return redirect('forum:readPost', post.pk)
    
    return render(request, 'forum/notLoggedIn.html', {'message':'hapus balasan'})