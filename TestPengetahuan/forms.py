from django import forms

from .models import Ganti

class gantiform(forms.ModelForm):
	class Meta:
		model = Ganti

		fields =[
			'a',
			'b',
			'c',
			'd',
		]

		labels = {
			'a' :'Jawaban Benar',
			'b' :'Jawaban Salah',
			'c' :'Jawaban Salah',
			'd' :'Jawaban Salah',
		}

		widgets = {
			'a' : forms.TextInput(attrs={'placeholder': 'Tulis Jawaban Benar','class':'form-control'}),
			'b' : forms.TextInput(attrs={'placeholder': 'Tulis Jawaban Salah','class':'form-control'}),
			'c' : forms.TextInput( attrs={'placeholder': 'Tulis Jawaban Salah','class':'form-control'}),
			'd' : forms.TextInput(attrs={'placeholder': 'Tulis Jawaban Salah','class':'form-control'}),
		}