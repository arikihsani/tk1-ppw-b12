from django.db import models

# Create your models here.

class Pertanyaan(models.Model):
	no = models.IntegerField(default=1)
	pertanyaan = models.CharField(max_length=200)

class Choice(models.Model):
    pertanyaan = models.CharField(max_length=20)
    choice = models.CharField(max_length=50)
    benarsalah = models.BooleanField(default=False)

class Nilai(models.Model):
	nilai = models.IntegerField(default=0)
	nama = models.CharField(max_length=50)

class Ganti(models.Model):
	a=models.CharField(max_length=20,default="")
	b=models.CharField(max_length=20,default="")
	c=models.CharField(max_length=20,default="")
	d=models.CharField(max_length=20,default="")