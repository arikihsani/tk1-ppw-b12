from django.db import models

# Create your models here.

class Pertanyaan(models.Model):
	no = models.IntegerField(default=1)
	pertanyaan = models.CharField(max_length=200)

class Opsi(models.Model):
    pertanyaan = models.OneToOneField(
        Pertanyaan,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    
    opsi1 = models.CharField(max_length=300, blank=True, default='')
    opsi2 = models.CharField(max_length=300, blank=True, default='')
    opsi3 = models.CharField(max_length=300, blank=True, default='')
    opsi4 = models.CharField(max_length=300, blank=True, default='')
    
    benarsalah1 = models.BooleanField(blank=True, default=False)
    benarsalah2 = models.BooleanField(blank=True, default=False)
    benarsalah3 = models.BooleanField(blank=True, default=False)
    benarsalah4 = models.BooleanField(blank=True, default=False)

class Skor(models.Model):
	skor = models.IntegerField(default=0)
