from django.urls import path

from . import views

app_name = 'HoaxSpreader'

urlpatterns = [
    path('home/', views.home, name='home'),
    path('skor/<int:cek>/', views.skor ),
    path('<int:id>/<int:cek>/', views.pertanyaan, name='pertanyaan' ),
    
]
