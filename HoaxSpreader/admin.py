from django.contrib import admin
from .models import Pertanyaan, Opsi, Skor

# Register your models here.
admin.site.register(Pertanyaan)
admin.site.register(Opsi)
admin.site.register(Skor)
