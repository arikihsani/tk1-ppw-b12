from django.apps import AppConfig


class HoaxspreaderConfig(AppConfig):
    name = 'HoaxSpreader'
